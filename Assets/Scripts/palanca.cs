﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class palanca : MonoBehaviour {


    public GameObject palankita;
    Rigidbody rb;
    private RigidbodyConstraints oFreeze;
    float originz;
    float originy;
    float force, forceup;
	void Start () {
        originz = this.transform.localPosition.z;
        originy = this.transform.localPosition.y;
        rb = this.gameObject.GetComponent<Rigidbody>();
        oFreeze = rb.constraints;
        force = 500f;
        forceup = 1000f;
  
	}

    void FixedUpdate() {
        if (Input.GetKey(KeyCode.Space))
        {
            moveDown();
        }
        else
        {
            moveUp();
        }

    }

    void moveDown()
    {
        rb.AddForce(-transform.forward * force);
        rb.constraints = oFreeze;

    }

    void moveUp()
    {
        if(this.transform.localPosition != new Vector3(this.transform.localPosition.x, originy, originz))
        { 
            rb.AddForce(transform.forward * forceup);
        }
        else
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="InitialPos")
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, originy, originz);
        }
    }
}
