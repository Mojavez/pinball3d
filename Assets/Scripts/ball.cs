﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour {

    public int counter;
    public score_balls ballsScore;
    public int valueBalls;
    Rigidbody rb;
    float originz;
    float originy;
    float originx;
    void Start () {
        counter = 3;
        valueBalls = 1;
        originz = this.transform.localPosition.z;
        originy = this.transform.localPosition.y;
        originx = this.transform.localPosition.x;
        rb = this.gameObject.GetComponent<Rigidbody>();
        ballsScore = GameObject.Find("num_balls").GetComponent<score_balls>();
    }
	
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "deadZone" && counter!=0)
        {
            counter--;
            this.transform.localPosition = new Vector3(originx, originy, originz);
            ballsScore.AddBalls(valueBalls);

            if (counter==0)
            {
                counter = 3;
                this.transform.localPosition = new Vector3(originx, -1.37f, originz);
            }
        }
    }

}
