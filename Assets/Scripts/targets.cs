﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class targets : MonoBehaviour {

    public score newScore;
    Collider col;
    public int numOfTargetsDown;
    float originz;
    float originy;
    float originx;
    private RigidbodyConstraints oFreeze;
    void Start () {
        col = this.gameObject.GetComponent<BoxCollider>();
        newScore = GameObject.Find("score_textNum").GetComponent<score>();
        numOfTargetsDown = 1;
        originz = this.transform.localPosition.z;
        originy = this.transform.localPosition.y;
        originx = this.transform.localPosition.x;


    }
	
	
	void Update () {
	}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            this.transform.localPosition = new Vector3(originx, -1f, originz);
            newScore.setTargetsDown(numOfTargetsDown);
            
        }
        
    }
}
