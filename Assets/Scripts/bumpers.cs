﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumpers : MonoBehaviour {

    public score newScore;
    private Rigidbody ballrb;
    private float force=100.0f;
    private float radius=1.0f;
    private Light light;
    public float valueScore;
	void Start () {
        newScore = GameObject.Find("score_textNum").GetComponent<score>();
        ballrb = GameObject.Find("ball").GetComponent<Rigidbody>();
        light = this.GetComponent<Light>();
        valueScore = 0.5f;
        
	}


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            foreach (Collider hit in Physics.OverlapSphere(transform.position, radius))
            {
                if (hit.GetComponent<Rigidbody>()!=null)
                {
                    hit.GetComponent<Rigidbody>().AddExplosionForce(force * ballrb.velocity.magnitude, transform.position, radius);
                }
            }
            newScore.AddScore(valueScore);
            light.intensity = 10;
            Invoke("setLight", 0.30f);
        }
    }

    void setLight()
    {
        light.intensity = 0;
    }
	
    

}
